{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<left>\n",
    "<table style=\"margin-top:0px; margin-left:0px;\">\n",
    "<tr>\n",
    "  <td><img src=\"https://gitlab.com/worm1/worm-figures/-/raw/master/style/worm.png\" alt=\"WORM\" title=\"WORM\" width=50/></td>\n",
    "  <td><h1 style=font-size:30px>Introduction to Estimating Thermodynamic Properties<br>of Aqueous Organic Compounds</h1><br />\n",
    "</tr>\n",
    "</table>\n",
    "<\\left>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### By Jordyn Robare and Grayson Boyer\n",
    "In this notebook, we will learn how the program AqOrg estimates thermodynamic properties of organic molecules, derives Helgeson-Kirkam-Flowers (HKF) parameters, and then extrapolates thermodynamic properties to non-standard conditions so that the compound can be written in reactions at conditions relevant to a research question.\n",
    "\n",
    "Email Jordyn at jrobare@asu.edu with any questions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import Packages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from AqOrg import Estimate\n",
    "from pyCHNOSZ import mod_OBIGT, add_OBIGT, reset, info, subcrt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1. Estimating thermodynamic properties using group contribution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The group contribution method of estimating thermodynamic properties first involves splitting a molecule into first- or second-order groups. First-order means that one functional group within a molecule is considered without accounting for the atoms the group is connected to. Second-order group contrubution considers each atom in a molecule and then everything that atom is directly bonded to. An example is shown below for **1-hexanol**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"https://gitlab.com/worm1/worm-figures/-/raw/master/4-Thermodynamic-Property-Estimation/1-Aq-Organics-Intro-Demo/Figure-1.png\" width=1000 height=600 style=\"background-color:white;padding:0px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, there are five CH2 groups in the first-order method. In this case, they would all be treated equally in terms of their thermodynamic properties. In the second-order method, one of these groups is treated differently than the others because it is bonded to one oxygen and one carbon instead of two carbons. This group will have different properties from the other CH2 groups. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Second-order syntax:\n",
    "\n",
    "- The group being considered is in square brackets.\n",
    "- H's are not considered their own group and can be included in the square brackets of the main group. \n",
    "- The atoms bonded to the main group are depicted after a hyphen. When there are more than one group bonded to the main group, the additional atoms are put in parentheses. It doesn't matter the order all the subsituents are listed. In fact, you may even see them before the main group. \n",
    "\n",
    "     Example: C-[CH3] is the same as [CH3]-C or [CH3]\\(-C)\n",
    "     \n",
    "Second-order groups are coded in SMARTS notation. For a thorough explanation of SMARTS notation, visit https://www.daylight.com/dayhtml/doc/theory/theory.smarts.html."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.1 Breaking a molecule into second-order groups"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can break a molecule into its groups using ```Estimate```. Change the molecule name to an organic molecule you want to see the second-order groups for and then run the cell. Remember to keep the molecule in quotations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/svg+xml": [
       "<svg baseProfile=\"full\" height=\"150px\" version=\"1.1\" width=\"450px\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:rdkit=\"http://www.rdkit.org/xml\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n",
       "<rect height=\"150\" style=\"opacity:1.0;fill:#FFFFFF;stroke:none\" width=\"450\" x=\"0\" y=\"0\"> </rect>\n",
       "<path d=\"M 20.4545,92.6442 81.5759,57.3558\" style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:2px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"/>\n",
       "<path d=\"M 81.5759,57.3558 142.697,92.6442\" style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:2px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"/>\n",
       "<path d=\"M 142.697,92.6442 203.818,57.3558\" style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:2px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"/>\n",
       "<path d=\"M 203.818,57.3558 264.94,92.6442\" style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:2px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"/>\n",
       "<path d=\"M 264.94,92.6442 326.061,57.3558\" style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:2px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"/>\n",
       "<path d=\"M 326.061,57.3558 350.127,71.25\" style=\"fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:2px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"/>\n",
       "<path d=\"M 350.127,71.25 374.192,85.1442\" style=\"fill:none;fill-rule:evenodd;stroke:#FF0000;stroke-width:2px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"/>\n",
       "<text style=\"font-size:15px;font-style:normal;font-weight:normal;fill-opacity:1;stroke:none;font-family:sans-serif;text-anchor:start;fill:#FF0000\" x=\"373.677\" y=\"100.144\"><tspan>OH</tspan></text>\n",
       "</svg>"
      ],
      "text/plain": [
       "<IPython.core.display.SVG object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>[C!H3]-[CX4H3R0]</th>\n",
       "      <th>C-[CX4H2R0]-C</th>\n",
       "      <th>O-[CX4H2]-C</th>\n",
       "      <th>C-[OX2H]</th>\n",
       "      <th>formula</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>hexanol</th>\n",
       "      <td>1</td>\n",
       "      <td>4</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>C6H14O</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "         [C!H3]-[CX4H3R0]  C-[CX4H2R0]-C  O-[CX4H2]-C  C-[OX2H] formula\n",
       "hexanol                 1              4            1         1  C6H14O"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Estimate('hexanol').group_matches"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After running the cell above, you should see your molecule, its second-order groups and the number of times they appear in the molecule you are testing, and the chemical formula of the molecule."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note: the X's shown after the main element in the group denotes connectivity. For example, CX4 means carbon has 4 bonds. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.2 Estimating properties from group contribution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to estimate properties of a molecule of interest using second-order group contribution, the contribution of each group is multiplied by the number of times they appears in the molecule and then added to get a total sum of contributions to that property. Each group, for example C-[CH3], will have its own value for the standard Gibbs energy of hydration $\\Delta_h$G$^{\\circ}$, the standard enthalpy of hydration $\\Delta_h$H$^{\\circ}$, the standard heat capacity of hydration $\\Delta_h$Cp$^{\\circ}$, and volume V$^{\\circ}_2$.\n",
    "\n",
    "For example, the hypothetical molecule shown below has two A groups and one B group. So, for each thermodynamic property, A's contribution is counted twice and B's contribution is counted once. Example property contributions are shown in the table."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"https://gitlab.com/worm1/worm-figures/-/raw/master/4-Thermodynamic-Property-Estimation/1-Aq-Organics-Intro-Demo/Figure-2.png\" width=500 height=600 style=\"background-color:white;padding:0px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"https://gitlab.com/worm1/worm-figures/-/raw/master/4-Thermodynamic-Property-Estimation/1-Aq-Organics-Intro-Demo/Figure-3.png\" width=400 height=600 style=\"background-color:white;padding:0px;\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can view all the group contribution values used in `Estimate` with `.group_data` or `.group_data.head()` to show only the first five groups in the table."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>group</th>\n",
       "      <th>Gh</th>\n",
       "      <th>Gh_err</th>\n",
       "      <th>Gh_n</th>\n",
       "      <th>Hh</th>\n",
       "      <th>Hh_err</th>\n",
       "      <th>Hh_n</th>\n",
       "      <th>Cph</th>\n",
       "      <th>Cph_err</th>\n",
       "      <th>Cp_count</th>\n",
       "      <th>V</th>\n",
       "      <th>V_err</th>\n",
       "      <th>V_count</th>\n",
       "      <th>refs</th>\n",
       "      <th>elem</th>\n",
       "      <th>published_notes</th>\n",
       "      <th>personal_notes 1</th>\n",
       "      <th>personal_notes 2</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>smarts</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>Yo</th>\n",
       "      <td>Yo</td>\n",
       "      <td>7.95</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-2.29</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1.12</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>Plyasunov et al. 2004a</td>\n",
       "      <td></td>\n",
       "      <td>NaN</td>\n",
       "      <td>material point</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>[C!H3]-[CX4H3R0]</th>\n",
       "      <td>C-(C)(H)3</td>\n",
       "      <td>3.72</td>\n",
       "      <td>0.07</td>\n",
       "      <td>136</td>\n",
       "      <td>-8.19</td>\n",
       "      <td>0.18</td>\n",
       "      <td>99</td>\n",
       "      <td>132</td>\n",
       "      <td>4</td>\n",
       "      <td>38</td>\n",
       "      <td>25.56</td>\n",
       "      <td>0.64</td>\n",
       "      <td>52</td>\n",
       "      <td>Plyasunov et al. 2004a</td>\n",
       "      <td>CH3</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>[CX4H3R0]-O</th>\n",
       "      <td>C-(O)(H)3</td>\n",
       "      <td>3.72</td>\n",
       "      <td>0.07</td>\n",
       "      <td>136</td>\n",
       "      <td>-8.19</td>\n",
       "      <td>0.18</td>\n",
       "      <td>99</td>\n",
       "      <td>132</td>\n",
       "      <td>4</td>\n",
       "      <td>38</td>\n",
       "      <td>25.56</td>\n",
       "      <td>0.64</td>\n",
       "      <td>52</td>\n",
       "      <td>Plyasunov et al. 2004a</td>\n",
       "      <td>CH3</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>C-[CX4H2R0]-C</th>\n",
       "      <td>C-(C)(H)2</td>\n",
       "      <td>0.68</td>\n",
       "      <td>0.03</td>\n",
       "      <td>96</td>\n",
       "      <td>-3.52</td>\n",
       "      <td>0.09</td>\n",
       "      <td>63</td>\n",
       "      <td>62</td>\n",
       "      <td>2</td>\n",
       "      <td>20</td>\n",
       "      <td>15.61</td>\n",
       "      <td>0.11</td>\n",
       "      <td>29</td>\n",
       "      <td>Plyasunov et al. 2004a</td>\n",
       "      <td>CH2</td>\n",
       "      <td>NaN</td>\n",
       "      <td>typo in table</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>C-[CX4H1](-C)-C</th>\n",
       "      <td>C-(C)(H)</td>\n",
       "      <td>-1.93</td>\n",
       "      <td>0.16</td>\n",
       "      <td>32</td>\n",
       "      <td>2.34</td>\n",
       "      <td>0.54</td>\n",
       "      <td>22</td>\n",
       "      <td>-17</td>\n",
       "      <td>10</td>\n",
       "      <td>2</td>\n",
       "      <td>5.96</td>\n",
       "      <td>0.8</td>\n",
       "      <td>8</td>\n",
       "      <td>Plyasunov et al. 2004a</td>\n",
       "      <td>CH</td>\n",
       "      <td>NaN</td>\n",
       "      <td>typo in table</td>\n",
       "      <td>NaN</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                      group     Gh Gh_err Gh_n     Hh Hh_err Hh_n  Cph  \\\n",
       "smarts                                                                   \n",
       "Yo                       Yo   7.95    NaN  NaN  -2.29    NaN  NaN    0   \n",
       "[C!H3]-[CX4H3R0]  C-(C)(H)3   3.72   0.07  136  -8.19   0.18   99  132   \n",
       "[CX4H3R0]-O       C-(O)(H)3   3.72   0.07  136  -8.19   0.18   99  132   \n",
       "C-[CX4H2R0]-C     C-(C)(H)2   0.68   0.03   96  -3.52   0.09   63   62   \n",
       "C-[CX4H1](-C)-C    C-(C)(H)  -1.93   0.16   32   2.34   0.54   22  -17   \n",
       "\n",
       "                 Cph_err Cp_count      V V_err V_count  \\\n",
       "smarts                                                   \n",
       "Yo                   NaN      NaN   1.12   NaN     NaN   \n",
       "[C!H3]-[CX4H3R0]       4       38  25.56  0.64      52   \n",
       "[CX4H3R0]-O            4       38  25.56  0.64      52   \n",
       "C-[CX4H2R0]-C          2       20  15.61  0.11      29   \n",
       "C-[CX4H1](-C)-C       10        2   5.96   0.8       8   \n",
       "\n",
       "                                    refs elem published_notes  \\\n",
       "smarts                                                          \n",
       "Yo                Plyasunov et al. 2004a                  NaN   \n",
       "[C!H3]-[CX4H3R0]  Plyasunov et al. 2004a  CH3             NaN   \n",
       "[CX4H3R0]-O       Plyasunov et al. 2004a  CH3             NaN   \n",
       "C-[CX4H2R0]-C     Plyasunov et al. 2004a  CH2             NaN   \n",
       "C-[CX4H1](-C)-C   Plyasunov et al. 2004a   CH             NaN   \n",
       "\n",
       "                 personal_notes 1 personal_notes 2  \n",
       "smarts                                              \n",
       "Yo                 material point              NaN  \n",
       "[C!H3]-[CX4H3R0]              NaN              NaN  \n",
       "[CX4H3R0]-O                   NaN              NaN  \n",
       "C-[CX4H2R0]-C       typo in table              NaN  \n",
       "C-[CX4H1](-C)-C     typo in table              NaN  "
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Estimate('hexanol', show=False).group_data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the cell below to use ```Estimate``` to estimate properties for your molecule using second-order group contribution. With the calculated thermodynamic properties, the program will estimate HKF (Helgeson-Kirkam-Flowers) parameters. Since the group contribution calculations were all done for standard conditions (25$^{\\circ}$C and 1 bar), we need a way to calculate these values at conditions that are relevant to our scientific investigations. For example, we may want to calculate whether a reaction is favorable in a hot spring that is 50$^{\\circ}$C."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "mod.OBIGT: updated hexanol(aq)\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "[915]"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "estimated_properties = Estimate('hexanol', show=False).OBIGT\n",
    "mod_OBIGT(estimated_properties)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can view the new thermodynamic data you estimated for your molecule using ```info(info(molecule))```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>name</th>\n",
       "      <th>abbrv</th>\n",
       "      <th>formula</th>\n",
       "      <th>state</th>\n",
       "      <th>ref1</th>\n",
       "      <th>ref2</th>\n",
       "      <th>date</th>\n",
       "      <th>E_units</th>\n",
       "      <th>G</th>\n",
       "      <th>H</th>\n",
       "      <th>...</th>\n",
       "      <th>Cp</th>\n",
       "      <th>V</th>\n",
       "      <th>a1</th>\n",
       "      <th>a2</th>\n",
       "      <th>a3</th>\n",
       "      <th>a4</th>\n",
       "      <th>c1</th>\n",
       "      <th>c2</th>\n",
       "      <th>omega</th>\n",
       "      <th>Z</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>915</th>\n",
       "      <td>hexanol</td>\n",
       "      <td>C6H14O</td>\n",
       "      <td>C6H14O</td>\n",
       "      <td>aq</td>\n",
       "      <td>AqOrg</td>\n",
       "      <td>GrpAdd</td>\n",
       "      <td>17/09/2021 15:18:20</td>\n",
       "      <td>J</td>\n",
       "      <td>-147480.0</td>\n",
       "      <td>-387470.0</td>\n",
       "      <td>...</td>\n",
       "      <td>613.200273</td>\n",
       "      <td>117.72</td>\n",
       "      <td>9.878325</td>\n",
       "      <td>7045.035804</td>\n",
       "      <td>119.933082</td>\n",
       "      <td>-467116.670978</td>\n",
       "      <td>581.936497</td>\n",
       "      <td>126553.0</td>\n",
       "      <td>-60209.117939</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>1 rows × 21 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "        name   abbrv formula state   ref1    ref2                 date  \\\n",
       "915  hexanol  C6H14O  C6H14O    aq  AqOrg  GrpAdd  17/09/2021 15:18:20   \n",
       "\n",
       "    E_units         G         H  ...          Cp       V        a1  \\\n",
       "915       J -147480.0 -387470.0  ...  613.200273  117.72  9.878325   \n",
       "\n",
       "              a2          a3             a4          c1        c2  \\\n",
       "915  7045.035804  119.933082 -467116.670978  581.936497  126553.0   \n",
       "\n",
       "            omega    Z  \n",
       "915 -60209.117939  0.0  \n",
       "\n",
       "[1 rows x 21 columns]"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "info(info(\"hexanol\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2. Writing a reaction at a specified temperature and pressure:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you have a molecule's properties and HKF parameters calculated, you can write reactions involving the molecule at various temperatures and pressures. Below we will calculate properties for the following reaction at 50$^\\circ$C and 1 bar pressure.\n",
    "\n",
    "<img src=\"https://gitlab.com/worm1/worm-figures/-/raw/master/4-Thermodynamic-Property-Estimation/1-Aq-Organics-Intro-Demo/Figure-4.png\" width=500 height=600 style=\"background-color:white;padding:20px;\"/>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "subcrt: 3 species at 50 ºC and 1 bar (wet) [energy units: cal]\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>coeff</th>\n",
       "      <th>name</th>\n",
       "      <th>formula</th>\n",
       "      <th>state</th>\n",
       "      <th>ispecies</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>915</th>\n",
       "      <td>-1</td>\n",
       "      <td>hexanol</td>\n",
       "      <td>C6H14O</td>\n",
       "      <td>aq</td>\n",
       "      <td>915.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>892</th>\n",
       "      <td>1</td>\n",
       "      <td>hexene</td>\n",
       "      <td>C6H12</td>\n",
       "      <td>aq</td>\n",
       "      <td>892.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>1</td>\n",
       "      <td>water</td>\n",
       "      <td>H2O</td>\n",
       "      <td>liq</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "     coeff     name formula state  ispecies\n",
       "915     -1  hexanol  C6H14O    aq     915.0\n",
       "892      1   hexene   C6H12    aq     892.0\n",
       "1        1    water     H2O   liq       1.0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>T</th>\n",
       "      <th>P</th>\n",
       "      <th>rho</th>\n",
       "      <th>logK</th>\n",
       "      <th>G</th>\n",
       "      <th>H</th>\n",
       "      <th>S</th>\n",
       "      <th>V</th>\n",
       "      <th>Cp</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>50.0</td>\n",
       "      <td>1</td>\n",
       "      <td>0.98803</td>\n",
       "      <td>-1.729962</td>\n",
       "      <td>2557.984274</td>\n",
       "      <td>7420.402254</td>\n",
       "      <td>15.024146</td>\n",
       "      <td>11.04337</td>\n",
       "      <td>-2.835852</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "      T  P      rho      logK            G            H          S         V  \\\n",
       "1  50.0  1  0.98803 -1.729962  2557.984274  7420.402254  15.024146  11.04337   \n",
       "\n",
       "         Cp  \n",
       "1 -2.835852  "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<pyCHNOSZ.fun.SubcrtOutput at 0x7feaf509d5c0>"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "subcrt([\"hexanol\", \"hexene\", \"water\"], [-1, 1, 1], T=50, P=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The standard Gibbs free energy of formation of hexanol is estimated to be about **2558 cal/mol** at 50$^\\circ$C and 1 bar."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3. Testing\n",
    "\n",
    "Now let's test if our estimations are close to experimental values. First let's pull up our estimated properties again. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>name</th>\n",
       "      <th>abbrv</th>\n",
       "      <th>formula</th>\n",
       "      <th>state</th>\n",
       "      <th>ref1</th>\n",
       "      <th>ref2</th>\n",
       "      <th>date</th>\n",
       "      <th>E_units</th>\n",
       "      <th>G</th>\n",
       "      <th>H</th>\n",
       "      <th>...</th>\n",
       "      <th>Cp</th>\n",
       "      <th>V</th>\n",
       "      <th>a1</th>\n",
       "      <th>a2</th>\n",
       "      <th>a3</th>\n",
       "      <th>a4</th>\n",
       "      <th>c1</th>\n",
       "      <th>c2</th>\n",
       "      <th>omega</th>\n",
       "      <th>Z</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>915</th>\n",
       "      <td>hexanol</td>\n",
       "      <td>C6H14O</td>\n",
       "      <td>C6H14O</td>\n",
       "      <td>aq</td>\n",
       "      <td>AqOrg</td>\n",
       "      <td>GrpAdd</td>\n",
       "      <td>17/09/2021 15:18:20</td>\n",
       "      <td>J</td>\n",
       "      <td>-147480.0</td>\n",
       "      <td>-387470.0</td>\n",
       "      <td>...</td>\n",
       "      <td>613.200273</td>\n",
       "      <td>117.72</td>\n",
       "      <td>9.878325</td>\n",
       "      <td>7045.035804</td>\n",
       "      <td>119.933082</td>\n",
       "      <td>-467116.670978</td>\n",
       "      <td>581.936497</td>\n",
       "      <td>126553.0</td>\n",
       "      <td>-60209.117939</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>1 rows × 21 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "        name   abbrv formula state   ref1    ref2                 date  \\\n",
       "915  hexanol  C6H14O  C6H14O    aq  AqOrg  GrpAdd  17/09/2021 15:18:20   \n",
       "\n",
       "    E_units         G         H  ...          Cp       V        a1  \\\n",
       "915       J -147480.0 -387470.0  ...  613.200273  117.72  9.878325   \n",
       "\n",
       "              a2          a3             a4          c1        c2  \\\n",
       "915  7045.035804  119.933082 -467116.670978  581.936497  126553.0   \n",
       "\n",
       "            omega    Z  \n",
       "915 -60209.117939  0.0  \n",
       "\n",
       "[1 rows x 21 columns]"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "info(info(\"hexanol\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the molecule we estimated already has thermodynamic data in our database, we can use ```reset()``` to get rid of our estimates and return to the original database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "reset: resetting \"thermo\" object\n",
      "\n",
      "OBIGT: loading default database with 1903 aqueous, 3443 total species\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>name</th>\n",
       "      <th>abbrv</th>\n",
       "      <th>formula</th>\n",
       "      <th>state</th>\n",
       "      <th>ref1</th>\n",
       "      <th>ref2</th>\n",
       "      <th>date</th>\n",
       "      <th>E_units</th>\n",
       "      <th>G</th>\n",
       "      <th>H</th>\n",
       "      <th>...</th>\n",
       "      <th>Cp</th>\n",
       "      <th>V</th>\n",
       "      <th>a1</th>\n",
       "      <th>a2</th>\n",
       "      <th>a3</th>\n",
       "      <th>a4</th>\n",
       "      <th>c1</th>\n",
       "      <th>c2</th>\n",
       "      <th>omega</th>\n",
       "      <th>Z</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>915</th>\n",
       "      <td>hexanol</td>\n",
       "      <td>N</td>\n",
       "      <td>C6H13OH</td>\n",
       "      <td>aq</td>\n",
       "      <td>SH90</td>\n",
       "      <td>SLOP16.7</td>\n",
       "      <td>1998-06-02</td>\n",
       "      <td>cal</td>\n",
       "      <td>-35490.0</td>\n",
       "      <td>-92690.0</td>\n",
       "      <td>...</td>\n",
       "      <td>144.4</td>\n",
       "      <td>118.65</td>\n",
       "      <td>1.78125</td>\n",
       "      <td>2748.61</td>\n",
       "      <td>12.591</td>\n",
       "      <td>-39152.0</td>\n",
       "      <td>127.661</td>\n",
       "      <td>57074.0</td>\n",
       "      <td>-55800.0</td>\n",
       "      <td>0.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>1 rows × 21 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "        name abbrv  formula state  ref1      ref2        date E_units  \\\n",
       "915  hexanol     N  C6H13OH    aq  SH90  SLOP16.7  1998-06-02     cal   \n",
       "\n",
       "           G        H  ...     Cp       V       a1       a2      a3       a4  \\\n",
       "915 -35490.0 -92690.0  ...  144.4  118.65  1.78125  2748.61  12.591 -39152.0   \n",
       "\n",
       "          c1       c2    omega    Z  \n",
       "915  127.661  57074.0 -55800.0  0.0  \n",
       "\n",
       "[1 rows x 21 columns]"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "reset()\n",
    "info(info(\"hexanol\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Do the values for $\\Delta_h$G$^{\\circ}$ look similar? Mind that they may be in different units: calories per mole or Joules per mole. The conversion between the two is 4.184 Joules = 1 calorie."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "915   -148490.16\n",
       "Name: G, dtype: float64"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "info(info(\"hexanol\"))['G']*4.184"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you plan to use your estimated properties further, re-run ```Estimate``` above and do not use the ```reset()``` function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
