{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<left>\n",
    "<table style=\"margin-top:0px; margin-left:0px;\">\n",
    "<tr>\n",
    "  <td><img src=\"https://gitlab.com/worm1/worm-figures/-/raw/master/style/worm.png\" alt=\"WORM\" title=\"WORM\" width=50/></td>\n",
    "  <td><h1 style=font-size:30px>Aqueous Geochemical Speciation</h1><h2>Exploration Notebook</h2></td>\n",
    "</tr>\n",
    "</table>\n",
    "<\\left>\n",
    "\n",
    "### Sample datasets:\n",
    "- <u>peru.csv</u> - hot spring samples from the Peruvian Andes ([Newell & Scott 2020](https://doi.org/10.26022/IEDA/111569))\n",
    "- <u>acidic_hotsprings.csv</u> - acidic hot springs from Yellowstone National Park\n",
    "- <u>leong2021.csv</u> - pH 7-12 samples from serpentinizing systems in Oman ([Leong *et al.* 2021](https://doi.org/10.1029/2020JB020756))\n",
    "- <u>S&C10vents.csv</u> - a variety of subsea hydrothermal vent fluids ([Shock & Canovas 2010](https://onlinelibrary.wiley.com/doi/full/10.1111/j.1468-8123.2010.00277.x))\n",
    "- <u>singlesample.csv</u> - a single sample from the leong2021 dataset. Use this CSV as a template for adding new rows of custom sample data!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ideas to try\n",
    "\n",
    "1) Open a sample dataset CSV and check out the kinds of variables used in a speciation calculation.\n",
    "2) Refresh and run this notebook from top to bottom with Kernel > Restart Kernel and Run All Cells.\n",
    "3) Look through the CSV that is generated ('report.csv' by default). How is it different than the sample dataset?\n",
    "4) Plot the mass contribution of a different basis species, like Ca+2.\n",
    "5) Try sort_by='Temperature' instead of 'pH' when creating a mass contribution plot. How does the plot change? What happens if you use another variable?\n",
    "6) Purposefully mis-name a basis species and check what the error message says.\n",
    "7) Plot the mineral saturation index of another sample.\n",
    "8) Plot different variables against each other with a scatterplot. Try mis-naming a variable to see which are available.\n",
    "9) Speciate another dataset. How did the results change?\n",
    "10) Plot the mass contribution of Fe+2 in the acidic_hotspring.csv sample set. What oxidation states are in the speciated complexes? How does this change if you add a column called 'Fe+3' (subheader 'Molality') in acidic_hotspring.csv, and assign a zero for each sample?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this cell makes plots appear larger\n",
    "import matplotlib.pyplot as plt\n",
    "plt.rcParams['figure.figsize'] = [10, 5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import AqEquil\n",
    "ae = AqEquil.AqEquil()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "speciation = ae.speciate(input_filename='peru.csv',\n",
    "                         exclude=['Year', 'Area'], # exclude metadata columns\n",
    "                         report_filename='report.csv') # create a CSV of results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# look up which sections are in the speciation report\n",
    "speciation.lookup()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# look up which variables are in a section\n",
    "speciation.lookup(\"charge_balance\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# look up a variable\n",
    "speciation.lookup(\"charge imbalance\").head() # .head() shows only the first 5 results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# look up multiple variables at once\n",
    "speciation.lookup([\"Temperature\", \"pH\", \"Na+\", \"Cl-\"]).head() # .head() shows only the first 5 results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "speciation.scatterplot('pH', 'Temperature')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "speciation.plot_mass_contribution('HCO3-', sort_by='pH', width=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "speciation.plot_mineral_saturation('DNCB17-21') # sample name"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "speciation.scatterplot('pH', ['CO3-2', 'HCO3-', 'CO2', 'Ca(HCO3)+'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "speciation.barplot(['Na+', 'Cl-'], plot_width=7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How-To Reference\n",
    "\n",
    "A few examples of how to get started with aqueous speciation on WORM are given below. For those with more Python experience, or those who wish to see the full range of options for each function, the documentation for the AqEquil Python package can be found [here](https://worm-portal.asu.edu/AqEquil-docs/AqSpeciation.html).\n",
    "\n",
    "---\n",
    "\n",
    "### Speciating water chemistry data\n",
    "\n",
    "Get the equilibration code loaded with:\n",
    "\n",
    "```python\n",
    "import AqEquil\n",
    "ae = AqEquil.AqEquil()\n",
    "```\n",
    "\n",
    "Speciate samples in 'myfile.csv' with:\n",
    "\n",
    "```python\n",
    "speciation = ae.speciate(input_filename='myfile.csv')\n",
    "```\n",
    "\n",
    "This creates the **speciation** object that stores the results. You can access these results in a few different ways, described below.\n",
    "\n",
    "---\n",
    "\n",
    "### Tables\n",
    "\n",
    "Look up which sections are in the speciation results:\n",
    "\n",
    "```python\n",
    "speciation.lookup()\n",
    "```\n",
    "\n",
    "Look up which variables are in a section of the results:\n",
    "\n",
    "```python\n",
    "speciation.lookup(\"aq_distribution\") # name of the section\n",
    "```\n",
    "\n",
    "Look up a desired variable with:\n",
    "\n",
    "```python\n",
    "speciation.lookup('O2')\n",
    "```\n",
    "\n",
    "Look up multiple variables at once by providing a *list* (several variable names separated by commas and enclosed in square brackets):\n",
    "\n",
    "```python\n",
    "speciation.lookup(['Mg+2', 'Mg(HCO3)+', 'Mg(HSiO3)+'])\n",
    "```\n",
    "\n",
    "Get the full speciation report with:\n",
    "\n",
    "```python\n",
    "speciation.report\n",
    "```\n",
    "\n",
    "The report might be truncated in the notebook. You can view the entire report by changing some settings with:\n",
    "\n",
    "```python\n",
    "import pandas as pd\n",
    "pd.set_option('display.max_rows', None, 'display.max_columns', None)\n",
    "speciation.report\n",
    "```\n",
    "\n",
    "You can undo this with:\n",
    "```python\n",
    "pd.reset_option('display')\n",
    "```\n",
    "\n",
    "Another way to view the entire report is by saving it to a CSV file during speciation with:\n",
    "\n",
    "```python\n",
    "speciation = ae.speciate(input_filename='myfile.csv',\n",
    "                         report_filename='myreport.csv')\n",
    "```\n",
    "\n",
    "The example above creates a file called 'myreport.csv' in the same directory as the Jupyter notebook.\n",
    "\n",
    "---\n",
    "\n",
    "### Visualization\n",
    "\n",
    "Create a pH-temperature scatterplot with:\n",
    "\n",
    "```python\n",
    "speciation.scatterplot()\n",
    "```\n",
    "\n",
    "Or plot two variables against each other by naming them:\n",
    "\n",
    "```python\n",
    "speciation.scatterplot('Na+', 'Cl-')\n",
    "```\n",
    "\n",
    "Plot multiple series along the same x-axis by providing a list:\n",
    "\n",
    "```python\n",
    "speciation.scatterplot('pH', ['CO2', 'HCO3-', 'CO3-2'])\n",
    "```\n",
    "\n",
    "Create a bar plot comparing a variable across all samples:\n",
    "\n",
    "```python\n",
    "speciation.barplot('Fe+2')\n",
    "```\n",
    "\n",
    "Create a grouped bar plot by providing a list of variables:\n",
    "\n",
    "```python\n",
    "speciation.barplot(['Fe+2', 'Fe+3'])\n",
    "```\n",
    "\n",
    "Plot the percent contribution of aqueous species to the mass balance of a desired *basis species* with:\n",
    "\n",
    "```python\n",
    "speciation.plot_mass_contribution('HCO3-')\n",
    "```\n",
    "\n",
    "Plot mineral saturation index of a sample with:\n",
    "\n",
    "```python\n",
    "speciation.plot_mineral_saturation('sample_name')\n",
    "```\n",
    "\n",
    "where 'sample_name' is the name of a sample.\n",
    "\n",
    "If a static plot is desired over an interactive one, you can turn off interactivity with `interactive=False`. This is valid for scatterplots and mass contribution plots.\n",
    "\n",
    "```python\n",
    "speciation.plot_mass_contribution('sample_name', interactive=False)\n",
    "```\n",
    "\n",
    "Save any kind of plot as a PNG by adding the parameter `save_as='my_figure'` to the plotting function.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Database download links:\n",
    "\n",
    "Download a CSV of the WORM database: https://worm-portal.asu.edu/wrm-db/wrm_data.csv\n",
    "\n",
    "Or in data0 format: https://worm-portal.asu.edu/wrm-db/data0.wrm.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
